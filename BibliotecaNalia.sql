-- phpMyAdmin SQL Dump
-- version 5.3.0-dev
-- https://www.phpmyadmin.net/
--
-- Host: 192.168.30.23
-- Tempo de geração: 05/10/2022 às 13:36
-- Versão do servidor: 8.0.18
-- Versão do PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `BibliotecaNalia`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `Autores`
--

CREATE TABLE `Autores` (
  `Id` int(10) UNSIGNED NOT NULL,
  `Nome` varchar(100) NOT NULL,
  `Data Nascimento` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Editoras`
--

CREATE TABLE `Editoras` (
  `Id` int(11) NOT NULL,
  `Nome` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Livros`
--

CREATE TABLE `Livros` (
  `Id` int(10) UNSIGNED NOT NULL,
  `Titulo` varchar(100) NOT NULL,
  `Data` date NOT NULL,
  `Id Autor` int(11) NOT NULL,
  `Id Editora` int(11) NOT NULL,
  `Id Local` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `Locais`
--

CREATE TABLE `Locais` (
  `Id` int(11) NOT NULL,
  `Estante` int(11) NOT NULL,
  `Prateleira` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tabelas despejadas
--

--
-- Índices de tabela `Autores`
--
ALTER TABLE `Autores`
  ADD PRIMARY KEY (`Id`);

--
-- Índices de tabela `Editoras`
--
ALTER TABLE `Editoras`
  ADD PRIMARY KEY (`Id`);

--
-- Índices de tabela `Livros`
--
ALTER TABLE `Livros`
  ADD PRIMARY KEY (`Id`);

--
-- Índices de tabela `Locais`
--
ALTER TABLE `Locais`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT para tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `Autores`
--
ALTER TABLE `Autores`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `Editoras`
--
ALTER TABLE `Editoras`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `Livros`
--
ALTER TABLE `Livros`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `Locais`
--
ALTER TABLE `Locais`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
